const { src,dest,watch }    = require('gulp'),
      { series,parallel}    = require('gulp'),
        del                 = require('del'),
        rename              = require('gulp-rename'),
        htmlmin             = require('gulp-htmlmin'),
        sass                = require('gulp-sass'),
        cleanCSS            = require('gulp-clean-css'),
        autoprefixer        = require('gulp-autoprefixer'),
        webpackStream       = require('webpack-stream'),
        uglify              = require('gulp-uglify'),
        imagemin            = require('gulp-imagemin'),
        mozjpeg             = require('imagemin-mozjpeg'),
        // pngquant            = require('imagemin-pngquant'), в Windows не работает
        webP                = require('imagemin-webp'),
        browserSync         = require('browser-sync').create();

// Пути
const path = {
    dest: {
        all:     'dist/**/*',
        root:    'dist/',
        img:     'dist/img/',
        css:     'dist/css/',
        js:      'dist/js/',
        fonts:   'dist/fonts/',
        favicon: 'dist/favicon/'
    },
    src: {
        all:     'src/**/*',
        root:    'src/',
        html:    'src/**/*.html',
        img:     'src/img/**/*.+(jpg|jpeg|png|gif|svg)',
        webp:    'src/img/**/*.+(jpg|jpeg|png)',
        scss:    'src/scss/style.scss',
        css:     'src/css/**/*.css',
        js:      'src/js/',
        fonts:   'src/fonts/**/*',
        favicon: 'src/favicon/**/*'
    },
    watch: {
        scss:     'src/scss/**/*.scss',
        js:       'src/js/index.js',
        html:     'src/**/*.html',
        img:      'src/img/**/*.+(jpg|jpeg|png|gif|svg)'
    }
}

// Задача работа с HTML

function html() {
    return src(path.src.html)
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(dest(path.dest.root))
}

// Задача SCSS in CSS

function scss() {
    return src(path.src.scss)
        .pipe(sass())
        .pipe(dest('src/css/'))
}

// Задача сжать CSS

function mincss() {
    return src(path.src.css)
        .pipe(autoprefixer())
        .pipe(cleanCSS({ level: 2 }))
        .pipe(dest(path.dest.css))
}

// Задача работы с JS через webpack

function minjs() {
    return src(path.src.js + 'index.js')
        .pipe(webpackStream({
            output: {
                filename: 'main.js'
            },
            mode: 'production',
            module: {
                rules: [{
                    test: /\.(js)$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                }]
            }
        }))
        .pipe(uglify({ toplevel: true }))
        .pipe(dest(path.dest.js))
}

// Версия для разработки

function devjs() {
    return src(path.src.js + 'index.js')
        .pipe(webpackStream({
            output: {
                filename: 'main.js'
            },
            mode: 'development',
            module: {
                rules: [{
                    test: /\.(js)$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                }]
            }
        }))
        .pipe(dest(path.src.js))
}


//Задача перемещения картинок
//добавить pngquant()

function img() {
    return src(path.src.img)
        .pipe(imagemin([
                mozjpeg({ quality: 75, progressive: true })
            ]))
        .pipe(dest(path.dest.img))
}

// Создание лучшие по сжатию картинки

function webp() {
    return src(path.src.webp)
        .pipe(imagemin([
                webP({ quality: 75 })
            ]))
        .pipe(rename({ extname: '.webp' }))
        .pipe(dest(path.dest.img))
}

// Задача перемещает папку с шрифтами

function fonts() {
    return src(path.src.fonts)
        .pipe(dest(path.dest.fonts));
}


//перемещение favicon

function favicon() {
    return src(path.src.favicon)
        .pipe(dest(path.dest.favicon))
}

// Очистка директории

function clean() {
    return del([path.dest.root])
}

function cleanDevJS() {
    return del([`src/js/main.js`])
}

// Задача создать рабочий сервер

function BrowserSync() {
    browserSync.init({
        server: {
            baseDir: 'src'
        }
    })
}

function BrowserSyncBuild() {
    browserSync.init({
        server: {
            baseDir: 'dist'
        }
    })
}

// функция перезагрузки

function reload(done) {
    browserSync.reload();
    done();
}

// Задача начать отслеживать файлы

function watchDev() {
    watch(path.watch.scss, series(scss, reload));
    watch(path.watch.js, series(devjs, reload));
    watch(path.watch.html, reload);
    watch(path.watch.img, reload);
}

// Задачи запуска через gulp [task]

exports.html     = html;
exports.scss     = scss;
exports.mincss   = mincss;
exports.minjs    = minjs;
exports.img      = img;
exports.webp     = webp;
exports.fonts    = fonts;
exports.favicon  = favicon;
exports.clean    = clean;
exports.cleandev = cleanDevJS;

exports.net      = BrowserSyncBuild;

// Задача разработки gulp live

exports.live     = series(
    scss,
    devjs,
    parallel(BrowserSync, watchDev)
);

//Создание проекта gulp build

exports.build = series(
    clean,
    cleanDevJS,
    parallel(
        html,
        series(scss, mincss),
        minjs,
        img,
        webp,
        fonts,
        favicon
    )
);