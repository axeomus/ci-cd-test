// File loaders section
import "./scss/style.scss";

// Start App
import Vue from 'vue/dist/vue.esm';
import data from './js/data';

let { main } = data;

new Vue({
    el: '.js-main__render',
    data: main,
    template: `
    <div class="row">
		<article v-for="item in article" :key="item.heading" class="main__works-item col-lg-4">
		    <div class="card w3-theme-l4">
		    	<picture class="card-img-top">
  					<source type="image/webp" :srcset="item.webp">
  					<img :src="item.img" alt="photo" class="img-fluid">
				</picture>
		        <div class="card-body">
		            <h5 class="card-title">{{item.heading}}</h5>
		            <p class="card-text">{{item.text}}</p>
		            <div class="main__works-link">
		                <a :href="item.url" class="btn btn-lg w3-theme-dark">Сотреть</a>
		            </div>
		        </div>
		    </div>
		</article>
    </div>
    `
});