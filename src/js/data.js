export default {
    article: [
        {
            img: 'img/04459.png',
            webp: 'img/04459.webp',
            heading: 'Страница 404',
            text: 'Шаблон страница с формой поиска.',
            url: '//axeomus.github.io/work/site1/Bootstrap'
        },
        {
            img: 'img/04478.jpg',
            webp: 'img/04478.webp',
            heading: 'S&S news',
            text: 'Шаблон новостного сайта.',
            url: '//axeomus.github.io/work/site2/Bootstrap'
        },
        {
            img: 'img/03189.jpg',
            webp: 'img/03189.webp',
            heading: 'Acrostia',
            text: 'Шаблон landig page портфолио студии. С интекативной навигацие.',
            url: '//axeomus.github.io/work/site3/Bootstrap'
        },
        {
            img: 'img/04745.jpg',
            webp: 'img/04745.webp',
            heading: 'Supernova',
            text: 'Шаблон новостной портал и подгрузки контента.',
            url: '//axeomus.github.io/work/site4/Bootstrap'
        },
        {
            img: 'img/04542.jpg',
            webp: 'img/04542.webp',
            heading: 'Strict',
            text: 'Шаблон landing page с работами и формой обратной связи.',
            url: '//axeomus.github.io/work/site5/Bootstrap'
        },
        {
            img: 'img/03215.png',
            webp: 'img/03215.webp',
            heading: 'Landing page',
            text: 'Шаблон landing page в стиле Material design.',
            url: '//axeomus.github.io/work/site6/Bootstrap'
        },
        {
            img: 'img/011.png',
            webp: 'img/011.webp',
            heading: 'Landing page',
            text: 'Шаблон landing page портфолио.',
            url: '//axeomus.github.io/websites/page1/Bootstrap'
        },
        {
            img: 'img/01603.png',
            webp: 'img/01603.webp',
            heading: 'Continuum',
            text: 'Стартовая страница с таймером открытия.',
            url: '//axeomus.github.io/websites/page2/Bootstrap'
        },
        {
            img: 'img/02548.jpg',
            webp: 'img/02548.webp',
            heading: 'Страница 404',
            text: 'Страница со статусом 404.',
            url: '//axeomus.github.io/websites/page3/Bootstrap'
        },
        {
            img: 'img/02347.jpg',
            webp: 'img/02347.webp',
            heading: 'WineStore',
            text: 'Шаблон винного магазина в темной теме.',
            url: '//axeomus.github.io/websites/page4/Bootstrap'
        },
        {
            img: 'img/02435.jpg',
            webp: 'img/02435.webp',
            heading: 'KINETICO',
            text: 'Шаблон блога оранжевого цвета.',
            url: '//axeomus.github.io/websites/page5/Bootstrap'
        },
        {
            img: 'img/04503.jpg',
            webp: 'img/04503.webp',
            heading: 'Avtomotive',
            text: 'Landing page написанный с технологий bootstrap 3 и pixel perfect.',
            url: '//axeomus.github.io/work/page7/Bootstrap'
        },
        {
            img: 'img/02918.png',
            webp: 'img/02918.webp',
            heading: 'Blog page',
            text: 'Шаблон блога для blogger.',
            url: '//axeomus.github.io/new_work_page/site1/HTML'
        },
        {
            img: 'img/03133.jpg',
            webp: 'img/03133.webp',
            heading: 'Mobile app',
            text: 'Страница регистрации мобильного приложения.',
            url: '//axeomus.github.io/new_work_page/site2/dest'
        },
        {
            img: 'img/03223.jpg',
            webp: 'img/03223.webp',
            heading: 'Media',
            text: 'Шаблон новостного тревел блога.',
            url: '//axeomus.github.io/new_work_page/site3/dest'
        }
    ]
}