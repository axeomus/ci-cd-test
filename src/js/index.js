import $ from 'jquery';
import 'lazysizes';

import data from './data';

$(function () {

	function template (data = '') {
		if (data) {
			let temp = `
				<article class="main__works-item col-lg-4">
				    <div class="card w3-theme-l4">
				    	<div class="card-img-top">
					    	<picture>
			  					<source type="image/webp" data-srcset="${data.webp}">
			  					<img data-src="${data.img}" alt="photo" class="img-fluid lazyload">
							</picture>
				    	</div>
				        <div class="card-body">
				            <h5 class="card-title">${data.heading}</h5>
				            <p class="card-text">${data.text}</p>
				            <div class="main__works-link">
				                <a href="${data.url}" class="btn btn-lg w3-theme-dark">Сотреть</a>
				            </div>
				        </div>
				    </div>
				</article>
			`
			return temp;
		}

		return '';
	}

	let { article } = data;
	let dom = '';
	let render = $('.js-render');

	console.log(article);

	dom = article.map(item => template(item));

	console.log(dom);

	render.html(dom);
})